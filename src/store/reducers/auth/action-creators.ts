import { AppDispatch } from "../..";
import {IUser} from "../../../models/IUser";
import {AuthActions, SetAuthAction, SetErrorAction, SetLoadingAction, SetUserAction} from "./types";
import axios from "axios";
import UserService from "../../../api/UserService";
export const AuthActionCreators =  {
    setUser: (user: IUser): SetUserAction => ({type: AuthActions.SET_USER, payload: user}),
    setIsAuth: (auth: boolean): SetAuthAction => ({type: AuthActions.SET_AUTH, payload: auth}),
    setIsLoading: (payload: boolean): SetLoadingAction => ({type: AuthActions.SET_IS_LOADING, payload}),
    setIsError: (payload: string): SetErrorAction => ({type: AuthActions.SET_ERROR, payload}),
    login: (username:string, password:string) => async (dispatch: AppDispatch) => {
        try {
            setTimeout(async () => {
                const response = await UserService.getUsers()
                const mockUser = response.data.find(user => user.username === username && user.password === password)
                console.log(mockUser)
                if (mockUser) {
                    localStorage.setItem("auth", "true")
                    localStorage.setItem("username", mockUser.username)
                    dispatch(AuthActionCreators.setIsAuth(true))
                    dispatch(AuthActionCreators.setUser(mockUser))
                } else {
                    dispatch(AuthActionCreators.setIsError("incorrect user name or password"))
                }
                dispatch(AuthActionCreators.setIsLoading(false))
            },1000)

        } catch (err){
            dispatch(AuthActionCreators.setIsError("Error"))
        }
    },
    logout: () => async (dispatch: AppDispatch) => {
            localStorage.removeItem("auth")
            localStorage.removeItem("username")
            dispatch(AuthActionCreators.setUser({} as IUser))
            dispatch(AuthActionCreators.setIsAuth(false))
    }
}