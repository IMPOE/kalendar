export const rules = {
    required: (message: string = "Required Field") => ({
        required: true,
        message
    })
}