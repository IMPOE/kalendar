import React, {FC, useEffect} from 'react';
import Navbar from "./components/Navbar";
import {Layout} from "antd";
import './App.css';
import {IUser} from "./models/IUser";
import AppRoutes from "./components/AppRoutes";
import {useAction} from "./hooks/useAction";

const App:FC = () => {
    const {setUser, setIsAuth} = useAction();

    useEffect(() => {
        if(localStorage.getItem('auth')) {
            setUser({username: localStorage.getItem('username' || '')} as IUser)
            setIsAuth(true);
        }
    }, [])

    return (
        <Layout>
            <Navbar/>
            <Layout.Content>
                <AppRoutes />
            </Layout.Content>
        </Layout>
    );
};

export default App;