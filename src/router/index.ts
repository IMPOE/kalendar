import React from "react";
import Login from "../pages/Login";
import Events from "../pages/Events";

export interface IRoutes{
    path: string;
    component: React.ComponentType;
    exact?: boolean;

}

export enum RouteNames {
    LOGIN = "/login",
    EVENTS = "/"

}

export  const publicRoutes: IRoutes[] = [
    {path: RouteNames.LOGIN, exact: true, component: Login}
]

export  const privateRoutes: IRoutes[] = [
    {path: RouteNames.EVENTS, exact: true, component: Events}
]