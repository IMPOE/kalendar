import React, {FC, useState} from 'react';
import {Button, Form, Input} from "antd";
import {rules} from "../utils/rules";
import {useTypedSelector} from "../hooks/useTypedSelector";
import {useAction} from "../hooks/useAction";

const LoginForm: FC = () => {
    const [userName, setUserName] = useState("")
    const [userPassword, setUserPassword] = useState("")
    const {error, isLoading} = useTypedSelector(state => state.auth)
    const {login} = useAction()
     const submit = () => {
        login(userName,userPassword);
    }
    return (
        <Form
            onFinish={submit}
        >
            {error && <div style={{color: "red"}}>
                {error}
            </div>
            }
            <Form.Item
                label="Username"
                name="username"
                rules={[rules.required('Please input your username!' )]}
            >
                <Input
                    value={userName}
                    onChange={ (e) => setUserName(e.target.value)}/>
            </Form.Item>
            <Form.Item
                label="Password"
                name="password"
                rules={[rules.required('Please input your password!')]}
            >
                <Input.Password
                    value={userPassword}
                    onChange={(e) => setUserPassword(e.target.value)} />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit" loading={isLoading}>
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
};

export default LoginForm;