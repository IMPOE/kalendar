import React, {FC} from 'react';
import {Layout, Menu, Row} from "antd";
import {useHistory} from "react-router-dom";
import {RouteNames} from "../router";
import {useTypedSelector} from "../hooks/useTypedSelector";
import {useAction} from "../hooks/useAction";
const Navbar:FC = () => {
    const router = useHistory()
    const {logout} = useAction()
    const {isAuth, user} = useTypedSelector(state => state.auth)
    return (
        <Layout.Header>
            <Row justify="end">
                {isAuth ?
                    <>
                    <div style={{color: "white"}}>
                        {user.username}
                    </div>
                    <Menu theme="dark"  mode="horizontal" selectable={false}>
                        <Menu.Item
                            onClick={logout}
                            key={1}
                        >
                            LogOut
                        </Menu.Item>
                    </Menu>
                    </>
                    :

                    <Menu theme="dark"  mode="horizontal" selectable={false}>
                        <Menu.Item
                            onClick={() => router.push(RouteNames.LOGIN)}
                            key={2}>LOgin</Menu.Item>
                    </Menu>

                }

            </Row>
        </Layout.Header>
    );
};

export default Navbar;